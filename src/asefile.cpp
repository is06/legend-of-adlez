#include <vector>
#include <SDL2/SDL.h>
#include "asefile.h"
#include "sprite.h"
#include "spritebatch.h"
#include "texture.h"

using namespace std;

AseFile::AseFile(AseHeader header, vector<AseFrame> frames)
{
    _header = header;
    _frames = frames;
}

Texture* AseFile::create_texture(SDL_Renderer* renderer) const
{
    auto sdl_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, 16, 16);

    unsigned char* pixels;
    int pitch;

    SDL_LockTexture(sdl_texture, nullptr, (void**)&pixels, &pitch);

    for (auto frame : _frames) {
        auto cel = (AseCelRaw*)frame.get_cel();
        int i = 0;
        for (auto pixel : cel->pixels) {
            auto p = static_cast<AsePixelRGBA&>(pixel);
            pixels[i] = p.data[0];
            i++;
        }
    }

    SDL_UnlockTexture(sdl_texture);

    return new Texture(sdl_texture);
}

AseCel* AseFrame::get_cel() const
{
    for (auto chunk : chunks) {
        if (chunk.type == AseChunkType::cel) {
            auto cel_chunk = static_cast<AseCelChunk*>(&chunk.data);
            return &cel_chunk->cel;
        }
    }
    return nullptr;
}
