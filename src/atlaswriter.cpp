#include <string>
#include "atlaswriter.h"
#include "asefile.h"
#include "gameexception.h"
#include "png.h"

using namespace std;

AtlasWriter::AtlasWriter()
{

}

void AtlasWriter::add_aseprite(AseFile* sprite)
{
    _aseprites.push_back(sprite);
}

void AtlasWriter::write_atlas(const string& file_path)
{
    auto write_struct = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (write_struct == NULL) {
        throw GameException("Unable to create png write struct");
    }
    auto info_struct = png_create_info_struct(write_struct);
    if (info_struct == NULL) {
        throw GameException("Unable to create png info struct");
    }
    png_bytep row = NULL;

    for (auto aseprite : _aseprites) {

    }
}
