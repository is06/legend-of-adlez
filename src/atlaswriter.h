#ifndef ATLASWRITER_H
#define ATLASWRITER_H

#include <string>
#include "asefile.h"

using namespace std;

class AtlasWriter
{
public:
    AtlasWriter();

    void add_aseprite(AseFile* sprite);
    void write_atlas(const string& file_path);

private:
    void write_image_data();
    void write_meta_data();

    vector<AseFile*> _aseprites;
};

#endif // ATLASWRITER_H
