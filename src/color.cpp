#include <SDL2/SDL.h>
#include "color.h"

Color::Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
{
    sdl_color = SDL_Color();
    sdl_color.r = red;
    sdl_color.g = green;
    sdl_color.b = blue;
    sdl_color.a = alpha;
}

Color Color::transparent = Color(0, 0, 0, 0);
Color Color::black = Color(0, 0, 0, 255);
Color Color::grey = Color(128, 128, 128, 255);
Color Color::white = Color(255, 255, 255, 255);
