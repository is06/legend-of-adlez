#ifndef COLOR_H
#define COLOR_H

#include <SDL2/SDL.h>

struct Color
{
    SDL_Color sdl_color;

    Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);

    static Color transparent;
    static Color black;
    static Color grey;
    static Color white;
};

#endif // COLOR_H
