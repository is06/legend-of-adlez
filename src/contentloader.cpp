#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include <png.h>
#include <stdio.h>
#include "asefile.h"
#include "contentloader.h"
#include "texture.h"

using namespace std;

ContentLoader::ContentLoader(SDL_Renderer* renderer, AseFileLoader* asefile_loader)
{
    _renderer = renderer;
    _asefile_loader = asefile_loader;
}

Texture* ContentLoader::create_texture(const string& file_path) const
{
    SDL_Surface* sdl_surface = IMG_Load(("content/" + file_path + ".png").c_str());
    if (sdl_surface == nullptr) {
        cerr << "Unable to find texture file '" << ("content/" + file_path + ".png") << "'" << endl;
    }
    SDL_Texture* sdl_texture = SDL_CreateTextureFromSurface(_renderer, sdl_surface);
    if (sdl_texture == nullptr) {
        cerr << "Unable to build texture from surface" << endl;
    }
    SDL_FreeSurface(sdl_surface);

    return new Texture(sdl_texture);
}

AseFile* ContentLoader::load_aseprite(const string& file_path) const
{
    return _asefile_loader->load(file_path);
}
