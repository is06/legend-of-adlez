#ifndef CONTENTLOADER_H
#define CONTENTLOADER_H

#include <SDL2/SDL.h>
#include <string>
#include "asefile.h"
#include "asefileloader.h"
#include "texture.h"

using namespace std;

class Texture;

class ContentLoader
{
public:
    ContentLoader(SDL_Renderer* renderer, AseFileLoader* asefile_loader);

    Texture* create_texture(const string& file_path) const;
    AseFile* load_aseprite(const string& file_path) const;

private:
    SDL_Renderer* _renderer;
    AseFileLoader* _asefile_loader;
};

#endif // CONTENTLOADER_H
