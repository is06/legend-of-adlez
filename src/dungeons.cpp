#include "dungeons.h"
#include "game.h"
#include "map.h"
#include "mapsectionlayout.h"

Dungeons::Dungeons(Game* game) : Map(game)
{
    // Default values
    for (unsigned char s = 0; s < 128; s++) {
        _section_layout_data[s] = MapSectionLayoutType::empty;
    }

    // Raw data
    // TODO: put in a static const array
    _section_layout_data[0] = MapSectionLayoutType::entrance;

    init_all_sections();
}

void Dungeons::init_all_sections()
{
    for (unsigned char s = 0; s < 128; s++) {
        auto section = MapSection();
        section.top_door = true;
        section.bottom_door = true;
        section.left_door = false;
        section.right_door = false;

        auto layout_type = _section_layout_data[s];
        section.layout = MapSectionLayout::create(layout_type);

        _sections[s] = section;
    }
}

void Dungeons::update()
{
    Map::update();
}

void Dungeons::draw()
{
    Map::draw();
}
