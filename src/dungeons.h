#ifndef DUNGEON1_H
#define DUNGEON1_H

#include "game.h"
#include "map.h"
#include "mapsectionlayout.h"

class Dungeons : public Map
{
public:
    Dungeons(Game* game);

    void update();
    void draw();

private:
    void init_all_sections();
};

#endif // DUNGEON1_H
