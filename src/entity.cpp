#include "entity.h"
#include "point.h"

Entity::Entity(Map* map) : _position(Point(0, 0))
{
    _map = map;
}

void Entity::update()
{

}

void Entity::draw()
{

}

void Entity::set_position(Point position)
{
    _position = position;
}

Entity::~Entity()
{

}
