#include <SDL2/SDL.h>
#include <iostream>
#include <thread>
#include "color.h"
#include "dungeons.h"
#include "game.h"
#include "gameexception.h"
#include "map.h"
#include "texture.h"

using namespace std;

/**
 * @brief Game constructor
 */
Game::Game()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        throw GameException("Unable to init SDL");
    }
    _window = SDL_CreateWindow("My Zelda NES",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               Game::width * Game::window_size,
                               Game::height * Game::window_size,
                               SDL_WINDOW_SHOWN);
    if (!_window) {
        throw GameException("Unable to create window");
    }

    _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);

    _content = new ContentLoader(_renderer, _asefile_loader);
    _graphics = new Graphics(_renderer);
    _sprite_batch = new SpriteBatch(_renderer);
    _current_map = new Dungeons(this);
    _in_game_frame_buffer = new Texture(SDL_CreateTexture(_renderer,
                                                          SDL_PIXELFORMAT_RGBA32,
                                                          SDL_TEXTUREACCESS_TARGET,
                                                          Game::width,
                                                          Game::height));

    _running = true;
}

/**
 * @brief Called at every frame to handle all game events
 */
void Game::update()
{
    if (_current_map != nullptr) {
        _current_map->update();
    }
}

/**
 * @brief Called at every frame to handle all game draws
 */
void Game::draw()
{
    _graphics->set_render_target(_in_game_frame_buffer);
    _graphics->clear_frame_buffer(Color::black);
    if (_current_map != nullptr) {
        _current_map->draw();
    }

    _graphics->set_render_target(nullptr);

    _sprite_batch->begin();
    _sprite_batch->draw(_in_game_frame_buffer,
                        Rectangle(0, 0, Game::width, Game::height),
                        Rectangle(0, 0, Game::width * Game::window_size, Game::height * Game::window_size));
    _sprite_batch->end();

    _graphics->end_render();
}

/**
 * @brief Quit the game
 */
void Game::quit()
{
    _running = false;
}

/**
 * @brief Get the content loader service
 * @return ContentLoader*
 */
ContentLoader* Game::content()
{
    return _content;
}

/**
 * @brief Get the sprite batch service
 * @return SpriteBatch*
 */
SpriteBatch* Game::sprite_batch()
{
    return _sprite_batch;
}

/**
 * @brief Game builder to call in main function
 */
void Game::run()
{
    SDL_Event events;

    Game game = Game();

    while (game._running) {
        while (SDL_PollEvent(&events)) {
            if (events.type == SDL_QUIT) {
                game._running = false;
            }
        }

        game.update();
        game.draw();

        this_thread::yield();
    }
}

/**
 * @brief Game destructor
 */
Game::~Game()
{
    delete _content;
    delete _graphics;
    delete _sprite_batch;

    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
    SDL_Quit();
}
