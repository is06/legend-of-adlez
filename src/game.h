#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include "contentloader.h"
#include "graphics.h"
#include "map.h"
#include "spritebatch.h"

class ContentLoader;
class Graphics;
class Map;
class SpriteBatch;

class Game
{
public:
    static void run();

    void quit();
    ContentLoader* content();
    SpriteBatch* sprite_batch();

private:
    Game();
    ~Game();

    static const unsigned char window_size = 3;
    static const unsigned short width = 256;
    static const unsigned short height = 224;

    void update();
    void draw();

    SDL_Window* _window;
    SDL_Renderer* _renderer;
    AseFileLoader* _asefile_loader;

    ContentLoader* _content;
    Graphics* _graphics;
    Map* _current_map;
    SpriteBatch* _sprite_batch;
    Texture* _in_game_frame_buffer;

    bool _running;
};

#endif // GAME_H
