#ifndef GAMEEXCEPTION_H
#define GAMEEXCEPTION_H

class GameException : public exception
{
public:
    GameException(const string& message) { m_message = message; }
    virtual const char* what() const throw() { return m_message.c_str(); }
    virtual ~GameException() {}
private:
    string m_message;
};

#endif // GAMEEXCEPTION_H
