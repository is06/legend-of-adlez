#include <SDL2/SDL.h>
#include "color.h"
#include "graphics.h"

Graphics::Graphics(SDL_Renderer* renderer)
{
    m_renderer = renderer;
}

void Graphics::set_render_target(Texture* render_target)
{
    if (render_target != nullptr) {
        SDL_SetRenderTarget(m_renderer, render_target->get_sdl_texture());
    } else {
        SDL_SetRenderTarget(m_renderer, nullptr);
    }
}

void Graphics::clear_frame_buffer(Color color)
{
    SDL_Color sdl_color = color.sdl_color;
    SDL_SetRenderDrawColor(m_renderer, sdl_color.r, sdl_color.g, sdl_color.b, sdl_color.a);
    SDL_RenderClear(m_renderer);
}

void Graphics::end_render()
{
    SDL_RenderPresent(m_renderer);
}

Graphics::~Graphics()
{
    SDL_DestroyRenderer(m_renderer);
}
