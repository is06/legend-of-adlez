#include "game.h"
#include "map.h"
#include "point.h"
#include "testentity.h"

unsigned char Map::_tileset[3][4] = {
    { // None
          0,   0,
          0,   0
    },
    { // Sand
          3,   3,
          3,   3
    },
    { // Square
          1,   2,
         17,  18
    },
};

Map::Map(Game* game)
{
    _game = game;
    _atlas = game->content()->create_texture("atlas");
    add_entity("test", new TestEntity(this));
    entity("test")->set_position(Point(32, 32));
}

void Map::update()
{
    for (auto entity : _entities) {
        entity.second->update();
    }
}

void Map::draw()
{
    draw_meta_tiles();

    for (auto entity : _entities) {
        entity.second->draw();
    }
}

void Map::draw_meta_tiles()
{
    int section_position_x = 0;
    int section_position_y = 0;
    // Draws sections
    for (auto section : _sections) {
        // Draws meta tiles in current section
        int meta_tile_position_x = 0;
        int meta_tile_position_y = 0;
        for (auto meta_tile_index : section.layout->_meta_tile_indexes) {
            // Draws tiles in current meta tile
            int offset_x = 0;
            int offset_y = 0;
            for (auto atlas_index : _tileset[meta_tile_index]) {
                int draw_x = section_position_x + meta_tile_position_x + offset_x;
                int draw_y = section_position_y + meta_tile_position_y + offset_y;

                // Draw only current section meta tiles
                if (draw_x >= 0 && draw_x < 256 && draw_y >= 0 && draw_y < 176) {
                    int source_y = atlas_index / 16;
                    int source_x = atlas_index - (source_y * 16);

                    _game->sprite_batch()->draw(
                        _atlas,
                        Rectangle(source_x * 8, source_y * 8, 8, 8),
                        Rectangle(draw_x, draw_y, 8, 8)
                    );
                }

                offset_x += 8;
                if (offset_x > 8) {
                    offset_x = 0;
                    offset_y += 8;
                }
            }
            meta_tile_position_x += 16;
            if (meta_tile_position_x > 176) {
                meta_tile_position_x = 0;
                meta_tile_position_y += 16;
            }
        }
        section_position_x += 4096;
        if (section_position_x > 256) {
            section_position_x = 0;
            section_position_y += 176;
        }
    }
}

void Map::add_entity(const string& name, Entity* entity)
{
    _entities[name] = entity;
}

Entity* Map::entity(const string& name)
{
    return _entities[name];
}

Game* Map::game()
{
    return _game;
}

Map::~Map()
{
    for (auto entity : _entities) {
        delete entity.second;
    }
}
