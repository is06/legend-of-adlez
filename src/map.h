#ifndef MAP_H
#define MAP_H

#include <unordered_map>
#include <string>
#include "entity.h"
#include "game.h"
#include "mapsection.h"

using namespace std;

class Entity;
class Game;

class Map
{
public:
    Map(Game* game);
    virtual ~Map();

    virtual void update();
    virtual void draw();

    void add_entity(const string& name, Entity* entity);
    Game* game();

protected:
    MapSectionLayoutType _section_layout_data[128];
    MapSection _sections[128];
    Entity* entity(const string& name);
    Texture* _atlas;

private:
    void draw_meta_tiles();

    static unsigned char _tileset[3][4];

    unordered_map<string, Entity*> _entities;
    Game* _game;
};

#endif // MAP_H
