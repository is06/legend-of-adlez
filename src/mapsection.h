#ifndef MAPSECTION_H
#define MAPSECTION_H

#include "mapsectionlayout.h"

struct MapSection
{
    MapSectionLayout* layout = nullptr;
    bool top_door = false;
    bool left_door = false;
    bool right_door = false;
    bool bottom_door = false;
};

#endif // MAPSECTION_H
