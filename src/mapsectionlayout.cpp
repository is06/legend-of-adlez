#include "mapsectionlayout.h"
#include "metatiletype.h"

unsigned char MapSectionLayout::layouts[3][84] = {
    { // empty
        2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    },
    { // entrance
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2,
        2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2,
        2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2,
    },
    { // single block
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    }
};

MapSectionLayout::MapSectionLayout()
{
    for (unsigned char i = 0; i < 84; i++) {
        _meta_tile_indexes[i] = MetaTileType::square;
    }
}

MapSectionLayout* MapSectionLayout::create(MapSectionLayoutType layout_type)
{
    auto layout = new MapSectionLayout();
    unsigned char i = 0;
    for (auto meta_tile_index : layouts[layout_type]) {
        layout->_meta_tile_indexes[i] = meta_tile_index;
        i++;
    }
    return layout;
}
