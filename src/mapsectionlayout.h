#ifndef MAPSECTIONLAYOUT_H
#define MAPSECTIONLAYOUT_H

#include "metatiletype.h"

enum MapSectionLayoutType
{
    empty = 0,
    entrance = 1,
    single_block = 2,
};

class MapSectionLayout
{
public:
    MapSectionLayout();
    void draw();

    static MapSectionLayout* create(MapSectionLayoutType type);
    static unsigned char layouts[3][84];

    unsigned char _meta_tile_indexes[84];
};

#endif // MAPSECTIONLAYOUT_H
