#ifndef METATILETYPE_H
#define METATILETYPE_H

enum MetaTileType
{
    none = 0,
    sand = 1,
    square = 2,
    block = 3,
    pit = 4,
    lava = 5,
    water = 6,
};

#endif // METATILETYPE_H
