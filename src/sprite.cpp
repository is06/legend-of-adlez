#include "point.h"
#include "size.h"
#include "sprite.h"
#include "spritebatch.h"
#include "texture.h"

Sprite::Sprite(SpriteBatch* sprite_batch, Texture* texture)
{
    _sprite_batch = sprite_batch;
    _texture = texture;
}

void Sprite::update()
{

}

void Sprite::draw(Point position)
{
    _sprite_batch->begin();
    _sprite_batch->draw(_texture,
                        Rectangle(Point(0, 0), Size(16, 16)),
                        Rectangle(position, Size(16, 16)));
    _sprite_batch->end();
}

Sprite::~Sprite()
{
    delete _texture;
}
