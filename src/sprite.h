#ifndef SPRITE_H
#define SPRITE_H

#include "spritebatch.h"
#include "texture.h"

class SpriteBatch;
class Texture;

class Sprite
{
public:
    Sprite(SpriteBatch* sprite_batch, Texture* texture);
    ~Sprite();

    void update();
    void draw(Point position);

private:
    SpriteBatch* _sprite_batch;
    Texture* _texture;
};

#endif // SPRITE_H
