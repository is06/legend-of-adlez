#include <SDL2/SDL.h>
#include "size.h"
#include "texture.h"

Texture::Texture(SDL_Texture* sdl_texture)
{
    _texture = sdl_texture;
}

SDL_Texture* Texture::get_sdl_texture() const
{
    return _texture;
}

Texture::~Texture()
{
    SDL_DestroyTexture(_texture);
    _texture = nullptr;
}
